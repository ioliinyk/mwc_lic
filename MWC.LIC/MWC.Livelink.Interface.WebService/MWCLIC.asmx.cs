﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Xml;
using System.Web.Services.Protocols;
using System.Collections.Specialized;


namespace MWC.Livelink.Interface.WebService
{
    /// <summary>
    /// Livelink Interface Component Web Service. Wrapper around the Livelink Interface Library
    /// </summary>
    [WebService(Name = "MWC LIC Web Service", Namespace = "http://www.melbournewater.com.au/webservices/", Description = "<b>Livelink Interface Component Web Service. v1.0.6.0</b>")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class MWCLIC : System.Web.Services.WebService
    {
        private LAPIConfigSettings GetLapiSettings()
        {
           return ConfigurationManager.GetSection("LAPIConfigSettings") as LAPIConfigSettings;
        }

        private LivelinkManager GetLivelinkManager(LivelinkSession session)
        {
            LAPIConfigSettings lapiSettings = GetLapiSettings();
            LivelinkManager livelinkManager = null;
            LivelinkInstance livelinkInstance = null;

            if (session.Authentication == LivelinkSession.AuthenticationType.Livelink)
            {
                livelinkInstance = LivelinkInstance.CreateLivelinkAuthenticationLivelinkInstance(lapiSettings.LivelinkServer, lapiSettings.LivelinkCGI, lapiSettings.LivelinkPort, session.LivelinkUser, session.Password, session.ImpersonateUser, string.Empty);
                livelinkManager = new LivelinkManager(livelinkInstance);
            }
            else if (session.Authentication == LivelinkSession.AuthenticationType.SingleSignOn)
            {
                livelinkInstance = LivelinkInstance.CreateNTLMAuthenticationLivelinkInstance(lapiSettings.LivelinkServer, lapiSettings.LivelinkCGI, lapiSettings.SSOPort, string.Empty, string.Empty, string.Empty);
                livelinkManager = new LivelinkManager(livelinkInstance);
            }
            else
            {
                throw RaiseException("GetLivelinkManager", new LivelinkConnectionException("Invalid authentication type."));
            }

            return livelinkManager;
        }

        private SoapException RaiseException(string method, Exception ex)
        {

            XmlDocument xmlDoc = new XmlDocument();
            //Create the Detail node
            XmlNode rootNode = xmlDoc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
            //Build specific details for the SoapException
            //Add first child of detail XML element.
            XmlNode errorNode = xmlDoc.CreateNode(XmlNodeType.Element, "Error", "http://www.melbournewater.com.au/webservices/");
            //Create and set the value for the ErrorCode node

            XmlNode errorCodeNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorCode", "http://www.melbournewater.com.au/webservices/");
            try
            {
                errorCodeNode.InnerText = ((LivelinkBaseException)ex).ExceptionCode.ToString();
            }
            catch
            { }
            //Create and set the value for the ErrorMessage node
            XmlNode errorMessageNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorMessage", "http://www.melbournewater.com.au/webservices/");
            errorMessageNode.InnerText = ex.Message;
            //Create and set the value for the ErrorSource node
            XmlNode errorSourceNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorSource", "http://www.melbournewater.com.au/webservices/");
            errorSourceNode.InnerText = ex.Source;
            //Create and set the value for the ErrorStackTrace node
            XmlNode errorStackTraceNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorStackTrace", "http://www.melbournewater.com.au/webservices/");
            errorStackTraceNode.InnerText = ex.StackTrace;
            //Create and set the value for the ErrorType node
            XmlNode errorTypeNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorType", "http://www.melbournewater.com.au/webservices/");
            errorTypeNode.InnerText = ex.GetType().ToString();

            //Append the Error child element nodes to the root detail node.
            errorNode.AppendChild(errorTypeNode);
            errorNode.AppendChild(errorCodeNode);
            errorNode.AppendChild(errorMessageNode);
            errorNode.AppendChild(errorSourceNode);
            errorNode.AppendChild(errorStackTraceNode);

            //Append the Detail node to the root node
            rootNode.AppendChild(errorNode);
            //Construct the exception
            SoapException soapEx = new SoapException(ex.Message, SoapException.ServerFaultCode, method, rootNode);
    
            //Raise the exception  back to the caller
            return soapEx;
        }

        #region Public Web methods

        /// <summary>
        /// Returns Livelink instance settings
        /// </summary>
        /// <returns>InstanceSettings</returns>
        [WebMethod]
        public InstanceSettings GetInstanceSettings()
        {
            try
            {
                LAPIConfigSettings lapiSettings = GetLapiSettings();
                InstanceSettings settings = new InstanceSettings();
                settings.LivelinkServer = lapiSettings.LivelinkServer;
                settings.LivelinkCGI = lapiSettings.LivelinkCGI;
                settings.LivelinkPort = lapiSettings.LivelinkPort;
                settings.SSOPort = lapiSettings.SSOPort;
                return settings;
            }
            catch (Exception ex)
            {
                throw RaiseException("GetInstanceSettings", ex);
            }
        }

        /// <summary>
        /// Returns all categories defined in the Livelink instance Category Volume
        /// </summary>
        /// <param name="session">Livelink connection information</param>
        /// <returns>CategoriesCollection</returns>
        [WebMethod]
        public CategoriesCollection GetInstanceCategories(LivelinkSession session)
        {
            try
            {
                LivelinkManager livelinkManager = GetLivelinkManager(session);
                return livelinkManager.GetInstanceCategories();
            }
            catch (Exception ex)
            {
                throw RaiseException("GetInstanceCategories", ex);
            }
        }

        /// <summary>
        /// Get a folder by its object ID
        /// </summary>
        /// <param name="session">Livelink connection information</param>
        /// <param name="folderID"> folder object ID</param>
        /// <returns>Folder</returns>
        [WebMethod]
        public Folder GetFolder(LivelinkSession session, int folderID)
        {
            try
            {
                LivelinkManager livelinkManager = GetLivelinkManager(session);
                return livelinkManager.GetFolder(folderID);
            }
            catch (Exception ex)
            {
                throw RaiseException("GetFolder", ex);
            }
        }

        /// <summary>
        /// Create Livelink folder object
        /// </summary>
        /// <param name="session">Livelink connection information</param>
        /// <param name="parentID">parent folder object ID</param>
        /// <param name="folderInfo">folder object containg all update information and categories to apply to the folder</param>
        /// <param name="uploadMode">overwrite if exists, read existing folder info or throw error</param>
        /// <returns>Folder</returns>
        [WebMethod]
        public Folder CreateFolder(LivelinkSession session, int parentID, Folder folderInfo, NodeExistsCreateMode uploadMode)
        {
            try
            {
                LivelinkManager livelinkManager = GetLivelinkManager(session);
                return livelinkManager.CreateFolder(parentID, folderInfo, uploadMode);
            }
            catch (Exception ex)
            {
                throw RaiseException("CreateFolder", ex);
            }
        }

        /// <summary>
        /// Create Livelink folder objects
        /// </summary>
        /// <param name="session">Livelink connection information</param>
        /// <param name="parentID">parent object ID</param>
        /// <param name="foldersInfo">folders collection object containg all folders update information and categories to apply to the folders</param>
        /// <param name="uploadMode">for each folder overwrite if exists, read existing folder info or throw error</param>
        /// <returns>FoldersCollection</returns>
        [WebMethod]
        public FoldersCollection CreateFolderStructure(LivelinkSession session, int parentID, FoldersCollection foldersInfo, NodeExistsCreateMode uploadMode)
        {
            try
            {
                LivelinkManager livelinkManager = GetLivelinkManager(session);
                return livelinkManager.CreateFolderStructure(parentID, foldersInfo, uploadMode);
            }
            catch (Exception ex)
            {
                throw RaiseException("CreateFolderStructure", ex);
            }
        }
        #endregion
    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MWC.Livelink.Interface.WebService.TestHarness
{
    public partial class CreateFolderStructure : CreateFolder
    {
        public CreateFolderStructure()
        {
            InitializeComponent();
        }

        protected override void DisplayFolderMenuItems()
        {
            if (treeViewFolder.SelectedNode == treeViewFolder.TopNode)
            {
                menuFolderRightClick.Items["addFolderMenuItem"].Visible = true;
                menuFolderRightClick.Items["addCategoryMenuItem"].Visible = false;
                menuFolderRightClick.Items["addAttributeMenuItem"].Visible = false;
                menuFolderRightClick.Items["editMenuItem"].Visible = false;
                menuFolderRightClick.Items["deleteMenuItem"].Visible = false;
            }
            else
            {
                menuFolderRightClick.Items["addFolderMenuItem"].Visible = true;
                menuFolderRightClick.Items["addCategoryMenuItem"].Visible = true;
                menuFolderRightClick.Items["addAttributeMenuItem"].Visible = false;
                menuFolderRightClick.Items["editMenuItem"].Visible = true;
                menuFolderRightClick.Items["deleteMenuItem"].Visible = true;
            }

        }

        protected override void AddFolder()
        {
            EntryForm entryForm = new EntryForm();
            entryForm.textBoxValue.Visible = false;
            entryForm.labelValue.Visible = false;
            DialogResult result = entryForm.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                if (folderInfo == null)
                {
                    folderInfo = new MWCLIC.Folder();
                    folderInfo.Name = "CreateFolderStructure";

                    folderInfo.Folders = new MWCLIC.Folder[1];
                    MWCLIC.Folder folder = new MWCLIC.Folder();
                    folder.Name = entryForm.textBoxName.Text;
                    folderInfo.Folders[0] = folder;
                }
                else
                {
                    MWCLIC.Folder curfolder = ((MWCLIC.Folder)treeViewFolder.SelectedNode.Tag);
                    MWCLIC.Folder[] folders = null;
                    if (curfolder.Folders == null)
                    {
                        folders = new MWCLIC.Folder[1];
                        MWCLIC.Folder folder = new MWCLIC.Folder();
                        folder.Name = entryForm.textBoxName.Text;
                        folders[0] = folder;
                    }
                    else
                    {
                        folders = new MWCLIC.Folder[curfolder.Folders.Length + 1];
                        int i = 0;
                        foreach (MWCLIC.Folder f in curfolder.Folders)
                        {
                            folders[i] = f;
                            i++;
                        }
                        MWCLIC.Folder folder = new MWCLIC.Folder();
                        folder.Name = entryForm.textBoxName.Text;
                        folders[i] = folder;
                    }
                    curfolder.Folders = folders;
                }
            }
            entryForm.Dispose();
            entryForm = null;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MWC.Livelink.Interface.WebService.TestHarness
{
    public partial class Encrypt : UserControl
    {
        public Encrypt()
        {
            InitializeComponent();
        }

        private void buttonEcrypt_Click(object sender, EventArgs e)
        {
            textEncryptedString.Text = StringEncryptor.EncryptData(textStringToEncrypt.Text);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textEncryptedString.Text = string.Empty;
            textStringToEncrypt.Text = string.Empty;
        }

    }
}

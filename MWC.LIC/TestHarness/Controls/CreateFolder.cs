﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MWC.Livelink.Interface.WebService.TestHarness
{
    public partial class CreateFolder : BaseControl
    {
        public MWCLIC.Folder folderInfo = null;

        public CreateFolder()
        {
            InitializeComponent();

            comboBoxNodeExistCreateMode.Items.Add(MWCLIC.NodeExistsCreateMode.Overwrite);
            comboBoxNodeExistCreateMode.Items.Add(MWCLIC.NodeExistsCreateMode.Read);
            comboBoxNodeExistCreateMode.Items.Add(MWCLIC.NodeExistsCreateMode.ReturnError);
        }

        #region "Right Click Menu"

        private void menuAddFolder_Click(object sender, System.EventArgs e)
        {
            AddFolder();
            DisplayTreeViewNodes();
        }
        private void addCategoryMenuItem_Click(object sender, EventArgs e)
        {
            EntryForm entryForm = new EntryForm();
            entryForm.textBoxValue.Visible = false;
            entryForm.labelValue.Visible = false;
            DialogResult result = entryForm.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                MWCLIC.Folder folder = ((MWCLIC.Folder)treeViewFolder.SelectedNode.Tag);
                MWCLIC.Category catInfo = new MWCLIC.Category();
                catInfo.Name = entryForm.textBoxName.Text;
                MWCLIC.Category[] cats = null;
                if (folder.Categories == null)
                {
                    cats = new MWCLIC.Category[1];
                    cats[0] = catInfo;
                }
                else
                {
                    cats = new MWCLIC.Category[folder.Categories.Length + 1];
                    int i = 0;
                    foreach (MWCLIC.Category cat in folder.Categories)
                    {
                        cats[i] = cat;
                        i++;
                    }
                    cats[i] = catInfo;
                }

                folder.Categories = cats;
            }
            entryForm.Dispose();
            entryForm = null;
            DisplayTreeViewNodes();

        }
        private void addAttributeMenuItem_Click(object sender, EventArgs e)
        {
            EntryForm entryForm = new EntryForm();
            DialogResult result = entryForm.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                MWCLIC.Attribute attrInfo = new MWCLIC.Attribute();
                attrInfo.Name = entryForm.textBoxName.Text;
                attrInfo.Value = entryForm.textBoxValue.Text;
                MWCLIC.Attribute[] atts = null;
                MWCLIC.Category cat = ((MWCLIC.Category)treeViewFolder.SelectedNode.Tag);
                if (cat.Attributes == null)
                {
                    atts = new MWCLIC.Attribute[1];
                    atts[0] = attrInfo;
                }
                else
                {
                    atts = new MWCLIC.Attribute[cat.Attributes.Length + 1];
                    int i = 0;
                    foreach (MWCLIC.Attribute att in cat.Attributes)
                    {
                        atts[i] = att;
                        i++;
                    }
                    atts[i] = attrInfo;
                }

                cat.Attributes = atts;
            }
            entryForm.Dispose();
            entryForm = null;
            DisplayTreeViewNodes();

        }
        private void editMenuItem_Click(object sender, EventArgs e)
        {
            if (treeViewFolder.SelectedNode == null) return;

            if (treeViewFolder.SelectedNode.Tag == null) return;
            
                        
            if (treeViewFolder.SelectedNode.Tag is MWCLIC.Folder)
            {
                MWCLIC.Folder folder = (MWCLIC.Folder)treeViewFolder.SelectedNode.Tag;
                EntryForm entryForm = new EntryForm();
                entryForm.textBoxValue.Visible = false;
                entryForm.labelValue.Visible = false;
                entryForm.textBoxName.Text = folder.Name;
                DialogResult result = entryForm.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    folder.Name = entryForm.textBoxName.Text;
                }
                entryForm.Dispose();
                entryForm = null;
            }
            else if (treeViewFolder.SelectedNode.Tag is MWCLIC.Category)
            {
                MWCLIC.Category cat = (MWCLIC.Category)treeViewFolder.SelectedNode.Tag;
                EntryForm entryForm = new EntryForm();
                entryForm.textBoxValue.Visible = false;
                entryForm.labelValue.Visible = false;
                entryForm.textBoxName.Text = cat.Name;
                DialogResult result = entryForm.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    cat.Name = entryForm.textBoxName.Text;
                }
                entryForm.Dispose();
                entryForm = null;
            }
            else if (treeViewFolder.SelectedNode.Tag is MWCLIC.Attribute)
            {
                MWCLIC.Attribute attr = (MWCLIC.Attribute)treeViewFolder.SelectedNode.Tag;
                EntryForm entryForm = new EntryForm();
                entryForm.textBoxName.Text = attr.Name;
                entryForm.textBoxValue.Text = attr.Value.ToString();
                DialogResult result = entryForm.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    attr.Name = entryForm.textBoxName.Text;
                    attr.Value = entryForm.textBoxValue.Text;
                }
                entryForm.Dispose();
                entryForm = null;
            }
            DisplayTreeViewNodes();


        }
        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            if (treeViewFolder.SelectedNode == null) return;

            if (treeViewFolder.SelectedNode.Tag == null) return;

            if (treeViewFolder.SelectedNode.Tag is MWCLIC.Folder)
            {
                MWCLIC.Folder folder = (MWCLIC.Folder)treeViewFolder.SelectedNode.Tag;
                MWCLIC.Folder parentFolder = (MWCLIC.Folder)treeViewFolder.SelectedNode.Parent.Tag;
                MWCLIC.Folder[] folders = new MWCLIC.Folder[parentFolder.Folders.Length - 1];
                int i = 0;
                foreach (MWCLIC.Folder f in parentFolder.Folders)
                {
                    if (folder.Name != f.Name)
                    {
                        folders[i] = f;
                        i++;
                    }
                }
                parentFolder.Folders = folders;
                if (treeViewFolder.SelectedNode.Parent == treeViewFolder.TopNode && parentFolder.Folders.Length == 0)
                {
                    folderInfo = null;
                }
            }
            else if (treeViewFolder.SelectedNode.Tag is MWCLIC.Category)
            {
                MWCLIC.Folder folder = (MWCLIC.Folder)treeViewFolder.SelectedNode.Parent.Tag;
                MWCLIC.Category cat = (MWCLIC.Category)treeViewFolder.SelectedNode.Tag;
                MWCLIC.Category[] cats = new MWCLIC.Category[folder.Categories.Length - 1];
                int i=0;
                foreach (MWCLIC.Category c in folder.Categories)
                {
                    if (cat.Name != c.Name)
                    {
                        cats[i] = c;
                        i++;
                    }
                }
                folder.Categories = cats;
            }
            else if (treeViewFolder.SelectedNode.Tag is MWCLIC.Attribute)
            {
                MWCLIC.Folder folder = (MWCLIC.Folder)treeViewFolder.SelectedNode.Parent.Parent.Tag;
                MWCLIC.Attribute attr = (MWCLIC.Attribute)treeViewFolder.SelectedNode.Tag;
                MWCLIC.Category[] cats = new MWCLIC.Category[folder.Categories.Length - 1];
                foreach (MWCLIC.Category c in folder.Categories)
                {
                    if (c.Attributes != null)
                    {
                        MWCLIC.Attribute[] atts = new MWCLIC.Attribute[c.Attributes.Length - 1];
                        int i = 0;
                        foreach (MWCLIC.Attribute a in c.Attributes)
                        {
                            if (attr.Name != a.Name)
                            {
                                atts[i] = a;
                                i++;
                            }
                        }
                        c.Attributes = atts;
                    }
                }
            }
            DisplayTreeViewNodes();
        }
        private void treeViewFolder_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (treeViewFolder.Nodes.Count > 0)
                {
                    if (treeViewFolder.SelectedNode == null) return;
                    if (treeViewFolder.SelectedNode.Tag == null)
                    {
                        DisplayEmptyTreeMenuItems();
                        menuFolderRightClick.Show(treeViewFolder, new Point(e.X, e.Y));

                    }
                    if (treeViewFolder.SelectedNode.Tag is MWCLIC.Folder)
                    {
                        DisplayFolderMenuItems();
                        menuFolderRightClick.Show(treeViewFolder, new Point(e.X, e.Y));

                    }
                    else if (treeViewFolder.SelectedNode.Tag is MWCLIC.Category)
                    {
                        DisplayCategoryMenuItems();
                        menuFolderRightClick.Show(treeViewFolder, new Point(e.X, e.Y));

                    }
                    else if (treeViewFolder.SelectedNode.Tag is MWCLIC.Attribute)
                    {
                        DisplayAttributeMenuItems();
                        menuFolderRightClick.Show(treeViewFolder, new Point(e.X, e.Y));
                    }
                    else
                    {
                    }
                }
            }
        }
        private void treeViewFolder_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            treeViewFolder.SelectedNode = e.Node;
        }
        protected virtual void AddFolder()
        {
            EntryForm entryForm = new EntryForm();
            entryForm.textBoxValue.Visible = false;
            entryForm.labelValue.Visible = false;
            DialogResult result = entryForm.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                folderInfo = new MWCLIC.Folder();
                folderInfo.Name = "CreateFolder";

                folderInfo.Folders = new MWCLIC.Folder[1];
                MWCLIC.Folder folder = new MWCLIC.Folder();
                folder.Name = entryForm.textBoxName.Text;
                folderInfo.Folders[0] = folder;

            }
            entryForm.Dispose();
            entryForm = null;

        }
        protected virtual void DisplayTreeViewNodes()
        {
            treeViewFolder.SuspendLayout();
            treeViewFolder.Nodes.Clear();

            if (folderInfo != null)
            {
                TreeNode topNode = new TreeNode(string.Format("{0}", folderInfo.Name));
                topNode.Tag = folderInfo;
                topNode = RecurseFolder(topNode, folderInfo);

                treeViewFolder.Nodes.Add(topNode);
            }
            else
            {
                treeViewFolder.Nodes.Add("Right Click here to add test folder...");
            }

            treeViewFolder.ExpandAll();
            treeViewFolder.ResumeLayout();
            treeViewFolder.Refresh();

        }

        private TreeNode RecurseFolder(TreeNode topNode, MWCLIC.Folder folder)
        {
            if (folder.Folders != null)
            {
                foreach (MWCLIC.Folder f in folder.Folders)
                {
                    TreeNode folderNode = new TreeNode(string.Format("[Folder] Name={0}", f.Name));
                    folderNode.Tag = f;

                    if (f.Categories != null)
                    {
                        foreach (MWCLIC.Category cat in f.Categories)
                        {
                            TreeNode catNode = new TreeNode(string.Format("[Category] Name={0}", cat.Name));
                            catNode.Tag = cat;
                            if (cat.Attributes != null)
                            {
                                foreach (MWCLIC.Attribute att in cat.Attributes)
                                {
                                    TreeNode attNode = new TreeNode(string.Format("[Attribute] Name={0} Value={1}", att.Name, att.Value));
                                    attNode.Tag = att;
                                    catNode.Nodes.Add(attNode);
                                }
                            }
                            folderNode.Nodes.Add(catNode);
                        }
                    }
                    topNode.Nodes.Add(RecurseFolder(folderNode, f));
                }
            }
            return topNode;
        }
        protected virtual void DisplayFolderMenuItems()
        {
            if (treeViewFolder.SelectedNode == treeViewFolder.TopNode)
            {
                menuFolderRightClick.Items["addFolderMenuItem"].Visible = false;
                menuFolderRightClick.Items["addCategoryMenuItem"].Visible = false;
                menuFolderRightClick.Items["addAttributeMenuItem"].Visible = false;
                menuFolderRightClick.Items["editMenuItem"].Visible = false;
                menuFolderRightClick.Items["deleteMenuItem"].Visible = false;
            }
            else
            {
                menuFolderRightClick.Items["addFolderMenuItem"].Visible = false;
                menuFolderRightClick.Items["addCategoryMenuItem"].Visible = true;
                menuFolderRightClick.Items["addAttributeMenuItem"].Visible = false;
                menuFolderRightClick.Items["editMenuItem"].Visible = true;
                menuFolderRightClick.Items["deleteMenuItem"].Visible = true;
            }
        }
        protected virtual void DisplayCategoryMenuItems()
        {
            menuFolderRightClick.Items["addFolderMenuItem"].Visible = false;
            menuFolderRightClick.Items["addCategoryMenuItem"].Visible = false;
            menuFolderRightClick.Items["addAttributeMenuItem"].Visible = true;
            menuFolderRightClick.Items["editMenuItem"].Visible = true;
            menuFolderRightClick.Items["deleteMenuItem"].Visible = true;
        }
        protected virtual void DisplayAttributeMenuItems()
        {
            menuFolderRightClick.Items["addFolderMenuItem"].Visible = false;
            menuFolderRightClick.Items["addCategoryMenuItem"].Visible = false;
            menuFolderRightClick.Items["addAttributeMenuItem"].Visible = false;
            menuFolderRightClick.Items["editMenuItem"].Visible = true;
            menuFolderRightClick.Items["deleteMenuItem"].Visible = true;
        }
        protected virtual void DisplayEmptyTreeMenuItems()
        {
            menuFolderRightClick.Items["addFolderMenuItem"].Visible = true;
            menuFolderRightClick.Items["addCategoryMenuItem"].Visible = false;
            menuFolderRightClick.Items["addAttributeMenuItem"].Visible = false;
            menuFolderRightClick.Items["editMenuItem"].Visible = false;
            menuFolderRightClick.Items["deleteMenuItem"].Visible = false;
        }
  
        #endregion

    }
}
﻿namespace MWC.Livelink.Interface.WebService.TestHarness
{
    partial class CreateFolder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Right Click here to add test folder...");
            this.label1 = new System.Windows.Forms.Label();
            this.treeViewFolder = new System.Windows.Forms.TreeView();
            this.menuFolderRightClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addFolderMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCategoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addAttributeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxParentID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxNodeExistCreateMode = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.menuFolderRightClick.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCancel.Location = new System.Drawing.Point(160, 594);
            // 
            // buttonRun
            // 
            this.buttonRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRun.Location = new System.Drawing.Point(79, 594);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Folder";
            // 
            // treeViewFolder
            // 
            this.treeViewFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeViewFolder.Location = new System.Drawing.Point(32, 177);
            this.treeViewFolder.Name = "treeViewFolder";
            treeNode1.Name = "testNode";
            treeNode1.Text = "Right Click here to add test folder...";
            this.treeViewFolder.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.treeViewFolder.Size = new System.Drawing.Size(298, 411);
            this.treeViewFolder.TabIndex = 13;
            this.treeViewFolder.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeViewFolder_MouseUp);
            this.treeViewFolder.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewFolder_NodeMouseClick);
            // 
            // menuFolderRightClick
            // 
            this.menuFolderRightClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addFolderMenuItem,
            this.addCategoryMenuItem,
            this.addAttributeMenuItem,
            this.editMenuItem,
            this.deleteMenuItem});
            this.menuFolderRightClick.Name = "menuFolderRightClick";
            this.menuFolderRightClick.Size = new System.Drawing.Size(148, 114);
            // 
            // addFolderMenuItem
            // 
            this.addFolderMenuItem.Name = "addFolderMenuItem";
            this.addFolderMenuItem.Size = new System.Drawing.Size(147, 22);
            this.addFolderMenuItem.Text = "Add Folder";
            this.addFolderMenuItem.Click += new System.EventHandler(this.menuAddFolder_Click);
            // 
            // addCategoryMenuItem
            // 
            this.addCategoryMenuItem.Name = "addCategoryMenuItem";
            this.addCategoryMenuItem.Size = new System.Drawing.Size(147, 22);
            this.addCategoryMenuItem.Text = "Add Category";
            this.addCategoryMenuItem.Click += new System.EventHandler(this.addCategoryMenuItem_Click);
            // 
            // addAttributeMenuItem
            // 
            this.addAttributeMenuItem.Name = "addAttributeMenuItem";
            this.addAttributeMenuItem.Size = new System.Drawing.Size(147, 22);
            this.addAttributeMenuItem.Text = "Add Attribute";
            this.addAttributeMenuItem.Click += new System.EventHandler(this.addAttributeMenuItem_Click);
            // 
            // editMenuItem
            // 
            this.editMenuItem.Name = "editMenuItem";
            this.editMenuItem.Size = new System.Drawing.Size(147, 22);
            this.editMenuItem.Text = "Edit";
            this.editMenuItem.Click += new System.EventHandler(this.editMenuItem_Click);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(147, 22);
            this.deleteMenuItem.Text = "Delete";
            this.deleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // textBoxParentID
            // 
            this.textBoxParentID.Location = new System.Drawing.Point(218, 124);
            this.textBoxParentID.Name = "textBoxParentID";
            this.textBoxParentID.Size = new System.Drawing.Size(112, 20);
            this.textBoxParentID.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Parent ID";
            // 
            // comboBoxNodeExistCreateMode
            // 
            this.comboBoxNodeExistCreateMode.FormattingEnabled = true;
            this.comboBoxNodeExistCreateMode.Location = new System.Drawing.Point(218, 150);
            this.comboBoxNodeExistCreateMode.Name = "comboBoxNodeExistCreateMode";
            this.comboBoxNodeExistCreateMode.Size = new System.Drawing.Size(112, 21);
            this.comboBoxNodeExistCreateMode.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(142, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "If Folder Exist";
            // 
            // CreateFolder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxNodeExistCreateMode);
            this.Controls.Add(this.textBoxParentID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.treeViewFolder);
            this.Controls.Add(this.label1);
            this.Name = "CreateFolder";
            this.Size = new System.Drawing.Size(353, 632);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.treeViewFolder, 0);
            this.Controls.SetChildIndex(this.buttonRun, 0);
            this.Controls.SetChildIndex(this.buttonCancel, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.textBoxParentID, 0);
            this.Controls.SetChildIndex(this.comboBoxNodeExistCreateMode, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.menuFolderRightClick.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem addFolderMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCategoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAttributeMenuItem;
        public System.Windows.Forms.TextBox textBoxParentID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox comboBoxNodeExistCreateMode;
        private System.Windows.Forms.ToolStripMenuItem editMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        protected System.Windows.Forms.TreeView treeViewFolder;
        protected System.Windows.Forms.ContextMenuStrip menuFolderRightClick;
    }
}

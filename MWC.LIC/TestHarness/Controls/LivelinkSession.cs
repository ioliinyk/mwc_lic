﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MWC.Livelink.Interface.WebService.TestHarness
{
    public partial class LivelinkSession : UserControl
    {
        public LivelinkSession()
        {
            InitializeComponent();
        }

        private void authentication_Changed(object sender, EventArgs e)
        {
            if (radioButtonLLA.Checked)
            {
                panelLivelinkAuthentication.Enabled = true;
            }
            else
            {
                panelLivelinkAuthentication.Enabled = false;
            }
        }

    }
}

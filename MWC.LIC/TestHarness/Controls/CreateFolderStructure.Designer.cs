﻿namespace MWC.Livelink.Interface.WebService.TestHarness
{
    partial class CreateFolderStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // treeViewFolder
            // 
            this.treeViewFolder.LineColor = System.Drawing.Color.Black;
            // 
            // CreateFolderStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "CreateFolderStructure";
            this.Controls.SetChildIndex(this.livelinkSession1, 0);
            this.Controls.SetChildIndex(this.treeViewFolder, 0);
            this.Controls.SetChildIndex(this.buttonRun, 0);
            this.Controls.SetChildIndex(this.buttonCancel, 0);
            this.Controls.SetChildIndex(this.textBoxParentID, 0);
            this.Controls.SetChildIndex(this.comboBoxNodeExistCreateMode, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}

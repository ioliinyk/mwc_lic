﻿namespace MWC.Livelink.Interface.WebService.TestHarness
{
    partial class LivelinkSession
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonLLA = new System.Windows.Forms.RadioButton();
            this.radioButtonSSO = new System.Windows.Forms.RadioButton();
            this.panelLivelinkAuthentication = new System.Windows.Forms.Panel();
            this.textBoxImpersonateUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxLivelinkUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panelLivelinkAuthentication.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonLLA);
            this.panel1.Controls.Add(this.radioButtonSSO);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(283, 21);
            this.panel1.TabIndex = 6;
            // 
            // radioButtonLLA
            // 
            this.radioButtonLLA.AutoSize = true;
            this.radioButtonLLA.Location = new System.Drawing.Point(116, 3);
            this.radioButtonLLA.Name = "radioButtonLLA";
            this.radioButtonLLA.Size = new System.Drawing.Size(132, 17);
            this.radioButtonLLA.TabIndex = 1;
            this.radioButtonLLA.Text = "Livelink Authentication";
            this.radioButtonLLA.UseVisualStyleBackColor = true;
            this.radioButtonLLA.CheckedChanged += new System.EventHandler(this.authentication_Changed);
            // 
            // radioButtonSSO
            // 
            this.radioButtonSSO.AutoSize = true;
            this.radioButtonSSO.Checked = true;
            this.radioButtonSSO.Location = new System.Drawing.Point(17, 3);
            this.radioButtonSSO.Name = "radioButtonSSO";
            this.radioButtonSSO.Size = new System.Drawing.Size(93, 17);
            this.radioButtonSSO.TabIndex = 0;
            this.radioButtonSSO.TabStop = true;
            this.radioButtonSSO.Text = "Single Sign on";
            this.radioButtonSSO.UseVisualStyleBackColor = true;
            this.radioButtonSSO.CheckedChanged += new System.EventHandler(this.authentication_Changed);
            // 
            // panelLivelinkAuthentication
            // 
            this.panelLivelinkAuthentication.Controls.Add(this.textBoxImpersonateUser);
            this.panelLivelinkAuthentication.Controls.Add(this.label3);
            this.panelLivelinkAuthentication.Controls.Add(this.textBoxPassword);
            this.panelLivelinkAuthentication.Controls.Add(this.label2);
            this.panelLivelinkAuthentication.Controls.Add(this.textBoxLivelinkUser);
            this.panelLivelinkAuthentication.Controls.Add(this.label1);
            this.panelLivelinkAuthentication.Enabled = false;
            this.panelLivelinkAuthentication.Location = new System.Drawing.Point(20, 30);
            this.panelLivelinkAuthentication.Name = "panelLivelinkAuthentication";
            this.panelLivelinkAuthentication.Size = new System.Drawing.Size(228, 89);
            this.panelLivelinkAuthentication.TabIndex = 11;
            // 
            // textBoxImpersonateUser
            // 
            this.textBoxImpersonateUser.Location = new System.Drawing.Point(105, 55);
            this.textBoxImpersonateUser.Name = "textBoxImpersonateUser";
            this.textBoxImpersonateUser.Size = new System.Drawing.Size(100, 20);
            this.textBoxImpersonateUser.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Impersonate User";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(105, 29);
            this.textBoxPassword.Multiline = true;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxPassword.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Password";
            // 
            // textBoxLivelinkUser
            // 
            this.textBoxLivelinkUser.Location = new System.Drawing.Point(105, 3);
            this.textBoxLivelinkUser.Name = "textBoxLivelinkUser";
            this.textBoxLivelinkUser.Size = new System.Drawing.Size(100, 20);
            this.textBoxLivelinkUser.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Livelink User";
            // 
            // LivelinkSession
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelLivelinkAuthentication);
            this.Controls.Add(this.panel1);
            this.Name = "LivelinkSession";
            this.Size = new System.Drawing.Size(300, 151);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelLivelinkAuthentication.ResumeLayout(false);
            this.panelLivelinkAuthentication.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel panel1;
        protected System.Windows.Forms.Panel panelLivelinkAuthentication;
        public System.Windows.Forms.RadioButton radioButtonLLA;
        public System.Windows.Forms.RadioButton radioButtonSSO;
        public System.Windows.Forms.TextBox textBoxImpersonateUser;
        public System.Windows.Forms.TextBox textBoxPassword;
        public System.Windows.Forms.TextBox textBoxLivelinkUser;
    }
}

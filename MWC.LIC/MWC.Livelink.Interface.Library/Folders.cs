﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Collection of Livelink folders
    /// </summary>
    [Serializable]
    public class FoldersCollection : List<Folder>
    {
        /// <summary>
        /// Folder indexer
        /// </summary>
        /// <param name="name">Folder name</param>
        /// <returns>Folder</returns>
        public Folder this[string name]
        {
            get
            {
                Folder folder = null;
                System.Collections.IEnumerator en = this.GetEnumerator();
                while (en.MoveNext())
                {
                    if (((Folder)en.Current).Name == name)
                    {
                        folder = (Folder)en.Current;
                        break;
                    }
                }
                return folder;
            }
        }
    }
}

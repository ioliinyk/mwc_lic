﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;


namespace MWC.Livelink.Interface
{
    internal class StringEncryptor
    {
        const string ENCRYPT_PASSWORD = "63f813281e55";
        /// <summary>
        /// Use to encrypt data string. The output string is the encrypted bytes as a base64 string.
        /// </summary>
        /// <param name="data">Clear string to encrypt.</param>
        /// <returns>Encrypted result as Base64 string.</returns>
        public static string EncryptData(string data)
        {
            byte[] encBytes = EncryptData(Encoding.UTF8.GetBytes(data), ENCRYPT_PASSWORD, PaddingMode.ISO10126);
            return Convert.ToBase64String(encBytes);
        }

        /// <summary>
        /// Decrypt the data string to the original string.  The data must be the base64 string
        /// returned from the EncryptData method.
        /// </summary>
        /// <param name="data">Encrypted data generated from EncryptData method.</param>
        /// <returns>Decrypted string.</returns>
        public static string DecryptData(string data)
        {
            byte[] encBytes = Convert.FromBase64String(data);
            byte[] decBytes = DecryptData(encBytes, ENCRYPT_PASSWORD, PaddingMode.ISO10126);
            return Encoding.UTF8.GetString(decBytes);
        }

        private static byte[] EncryptData(byte[] data, string password, PaddingMode paddingMode)
        {

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, Encoding.UTF8.GetBytes("Salt"));
            RijndaelManaged rm = new RijndaelManaged();
            rm.Padding = paddingMode;
            ICryptoTransform encryptor = rm.CreateEncryptor(pdb.GetBytes(16), pdb.GetBytes(16));

            using (MemoryStream msEncrypt = new MemoryStream())
            using (CryptoStream encStream = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
            {
                encStream.Write(data, 0, data.Length);
                encStream.FlushFinalBlock();
                return msEncrypt.ToArray();
            }
        }

        private static byte[] DecryptData(byte[] data, string password, PaddingMode paddingMode)
        {

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, Encoding.UTF8.GetBytes("Salt"));
            RijndaelManaged rm = new RijndaelManaged();
            rm.Padding = paddingMode;
            ICryptoTransform decryptor = rm.CreateDecryptor(pdb.GetBytes(16), pdb.GetBytes(16));

            using (MemoryStream msDecrypt = new MemoryStream(data))
            using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
            {
                // Decrypted bytes will always be less then encrypted bytes, so len of encrypted data will be big enouph for buffer.
                byte[] fromEncrypt = new byte[data.Length];

                // Read as many bytes as possible.
                int read = csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
                if (read < fromEncrypt.Length)
                {
                    // Return a byte array of proper size.
                    byte[] clearBytes = new byte[read];
                    Buffer.BlockCopy(fromEncrypt, 0, clearBytes, 0, read);
                    return clearBytes;
                }
                return fromEncrypt;
            }
        }
    }
}

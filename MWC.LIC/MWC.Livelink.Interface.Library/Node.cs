using System;
using System.Collections.Generic;
using System.Text;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Class representing base Livelink Node object.
    /// All Livelink objects inherit this object.
    /// </summary>
    [Serializable]
    public abstract class Node
    {
       
        #region Constructors
        protected Node()
        {
            _categories = new CategoriesCollection();
        }
        protected Node(int ID)
        {
            _id = ID;
            _categories = new CategoriesCollection();
        }
        protected Node(string Name)
        {
            _name = Name;
            _categories = new CategoriesCollection();
        }
        protected Node(int ID, string Name)
        {
            _id = ID;
            _name = Name;
            _categories = new CategoriesCollection();
        }
        protected Node(int VolumeID, int ID)
        {
            _id = ID;
            _volumeID = VolumeID;
            _categories = new CategoriesCollection();
        }

        #endregion

        #region Fields
        private int _id;
        private int _volumeID;
        private int _parentID;
        private string _name;
        private DateTime _created;
        private DateTime _modified;
        private CategoriesCollection _categories;
        #endregion

        #region Properties
        /// <summary>
        /// Node ID
        /// </summary>
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        /// <summary>
        /// Node Volume ID
        /// </summary>
        public int VolumeID
        {
            get
            {
                return _volumeID;
            }
            set
            {
                _volumeID = value;
            }
        }

        /// <summary>
        /// Node Parent ID
        /// </summary>
        public int ParentID
        {
            get
            {
                return _parentID;
            }
            set
            {
                _parentID = value;
            }
        }

        /// <summary>
        /// Node Name
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// Node Created Date
        /// </summary>
        public DateTime CreatedDate
        {
            get
            {
                return _created;
            }
            set
            {
                _created = value;
            }
        }

        /// <summary>
        /// Node Modified date
        /// </summary>
        public DateTime ModifiedDate
        {
            get
            {
                return _modified;
            }
            set
            {
                _modified = value;
            }
        }

        /// <summary>
        /// Node categories
        /// </summary>
        /// 
        public CategoriesCollection Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                _categories = value;
            }
        }
        #endregion
   
    }
}

using System;
using System.Collections.Generic;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Collection of Livelink categories
    /// </summary>
    [Serializable]
    public class CategoriesCollection : List<Category>
    {
        /// <summary>
        /// Category indexer
        /// </summary>
        /// <param name="name">Category name</param>
        /// <returns>Category</returns>
        public Category this[string name]
        {
            get
            {
                Category cat = null;
                System.Collections.IEnumerator en = this.GetEnumerator();
                while (en.MoveNext())
                {
                    if (((Category)en.Current).Name == name)
                    {
                        cat = (Category)en.Current;
                        break;
                    }
                }
                return cat;
            }
        }
    }
}

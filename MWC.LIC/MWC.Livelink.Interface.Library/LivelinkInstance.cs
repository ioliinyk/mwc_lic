using System;
using System.Collections.Generic;
using System.Text;


namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Class representing Livelink instance connection information.
    /// </summary>
    [Serializable]
    public class LivelinkInstance
    {
        private LivelinkInstance()
        { }

        private LivelinkInstance(string livelinkServer, string livelinkCGI, int livelinkPortNo, string livelinkUserName, string livelinkUserPassword, string impersonateLivelinkUser, string httpUser, string httpUserPassword, bool livelinkSingleSignOnEnabled, string livelinkDBConnectionString)
        {
            _livelinkserver = livelinkServer;
            _livelinkcgi = livelinkCGI;
            _llportno = livelinkPortNo;
            _llusername = livelinkUserName;
            _llpassword = livelinkUserPassword;
            _impersonateuser = impersonateLivelinkUser;
            _connectionstring = livelinkDBConnectionString;
            _httpusername = httpUser;
            _httppassword = httpUserPassword;
            _singlesignonenabled = livelinkSingleSignOnEnabled;
        }

        /// <summary>
        /// Initialise Livelink instance using Livelink authentication.
        /// </summary>
        /// <param name="livelinkServer">Livelink instance server name. Example: briwsqa</param>
        /// <param name="livelinkCGI">Livelink CGI url. Example: /livelink/livelink.exe</param>
        /// <param name="livelinkPortNo">Livelink port Number. Example: 80</param>
        /// <param name="livelinkUserName">Livelink user name</param>
        /// <param name="livelinkUserPassword">Livelink user password</param>
        /// <param name="impersonateLivelinkUser">Livelink user to impersonate</param>
        /// <param name="livelinkDBConnectionString">Livelink databse connection string for direct DB calls.</param>
        /// <returns>LivelinkInstance</returns>
        public static LivelinkInstance CreateLivelinkAuthenticationLivelinkInstance(string livelinkServer, string livelinkCGI, int livelinkPortNo, string livelinkUserName, string livelinkUserPassword, string impersonateLivelinkUser, string livelinkDBConnectionString)
        {
           return new LivelinkInstance(livelinkServer, livelinkCGI, livelinkPortNo, livelinkUserName, livelinkUserPassword, impersonateLivelinkUser, string.Empty, string.Empty, false,  livelinkDBConnectionString);
        }
        /// <summary>
        /// Initialise Livelink instance using NTLM authentication. With this instance currently logged in AD user will be used to authenticate with Livelink.
        /// Livelink and IIS have to be  already configured to use Single Sign On for this to work.
        /// </summary>
        /// <param name="livelinkServer">Livelink instance server name. Example: briwsqa</param>
        /// <param name="livelinkCGI">Livelink CGI url. Example: /livelink/livelink.exe</param>
        /// <param name="livelinkPortNo">Livelink port Number. Example: 80</param>
        /// <param name="httpUser">Windows user name to authenticate with IIS. Usually blank, only used if want to connect as different user</param>
        /// <param name="httpUserPassword">Http user password</param>
        /// <param name="livelinkDBConnectionString">Livelink database connection string for direct DB calls.</param>
        /// <returns>LivelinkInstace</returns>
        public static LivelinkInstance CreateNTLMAuthenticationLivelinkInstance(string livelinkServer, string livelinkCGI, int livelinkPortNo, string httpUser, string httpUserPassword, string livelinkDBConnectionString)
        {
            return new LivelinkInstance(livelinkServer, livelinkCGI, livelinkPortNo, string.Empty, string.Empty, string.Empty, httpUser, httpUserPassword, true, livelinkDBConnectionString);
        }
        #region Properties

        private string _livelinkserver;
        private string _livelinkcgi;
        private int _llportno;
        private string _llusername;
        private string _llpassword;
        private string _impersonateuser;
        private string _connectionstring;
        private bool _singlesignonenabled;
        private string _httpusername;
        private string _httppassword;

        

        public string LivelinkServer
        {
            get { return _livelinkserver; }
            set { _livelinkserver = value; }
        }
        public string LivelinkCGI
        {
            get { return _livelinkcgi; }
            set { _livelinkcgi = value; }
        }
        public int LivelinkPortNo
        {
            get { return _llportno; }
            set { _llportno = value; }
        }
        public string LivelinkUserName
        {
            get { return _llusername; }
            set {_llusername = value; }
        }
        public string LivelinkUserPassword
        {
            get { return _llpassword; }
            set { _llpassword = value; }
        }
        public string ImpersonateLivelinkUser
        {
            get { return _impersonateuser; }
            set { _impersonateuser = value; }
        }
        public string LivelinkDBConnectionString
        {
            get { return _connectionstring; }
            set { _connectionstring = value; }
        }
        public bool LivelinkSingleSignOnEnabled
        {
            get { return _singlesignonenabled; }
            set { _singlesignonenabled = value; }
        }
        public string HTTPUserName
        {
            get { return _httpusername; }
            set { _httpusername = value; }
        }
        public string HTTPPassword
        {
            get { return _httppassword; }
            set { _httppassword = value; }
        }

        #endregion
    }
}

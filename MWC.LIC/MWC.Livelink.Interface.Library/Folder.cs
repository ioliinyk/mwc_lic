using System;
using System.Collections.Generic;
using System.Text;
using MWC.Livelink.Interface;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Class representing Livelink Folder object.
    /// </summary>
    [Serializable]
    public class Folder : Node
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Folder() : base()
        {
            _folders = new FoldersCollection();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ID">Folder ID</param>
        public Folder(int ID)
            : base(ID)
        {
            _folders = new FoldersCollection();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Name">Folder Name</param>
        public Folder(string Name) : base(Name)
        {
            _folders = new FoldersCollection();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ID">Folder ID</param>
        /// <param name="Name">Folder Name</param>
        public Folder(int ID, string Name)
            : base(ID, Name)
        {
            _folders = new FoldersCollection();

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="VolumeID">Folder Volume ID</param>
        /// <param name="ID">Folder ID</param>
          public Folder(int VolumeID, int ID)
            : base(VolumeID, ID)
        {
            _folders = new FoldersCollection();
        }
        #endregion

        #region properties
          private FoldersCollection _folders;
          public FoldersCollection Folders
          {
              get 
              {
                  return _folders;
              }
              set
              {
                  _folders = value;
              }
          }
        #endregion
    }
}

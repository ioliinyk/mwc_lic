using System;
using System.Collections.Generic;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Collection of Livelink category attributes
    /// </summary>
    [Serializable]
    public class AttributesCollection : List<Attribute>
    {
        /// <summary>
        /// Attributes indexer
        /// </summary>
        /// <param name="name">Attribute name</param>
        /// <returns>Attribute</returns>
        public Attribute this[string name]
        {
            get
            {
                Attribute attr = null;
                System.Collections.IEnumerator en = this.GetEnumerator();
                while (en.MoveNext())
                {
                    if (((Attribute)en.Current).Name == name)
                    {
                        attr = (Attribute)en.Current;
                        break;
                    }
                }
                return attr;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using com.opentext.api;
using java.lang;
using System.IO;
using System.Collections;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Action enum for Node creation if Node already exists.
    /// -Overwire - will update the node
    /// -Read - will read the node information and return
    /// -ReturnError - will return error if node already exist
    /// </summary>
    public enum NodeExistsCreateMode
    {
        Overwrite,
        Read,
        ReturnError
    }

    /// <summary>
    /// Main class that exposes public functions for manipulating Livelink objects.
    /// This class also handles all LAPI requests.
    /// </summary>
    public class LivelinkManager : IDisposable
    {
        private Guid _id;
        private LLSession LivelinkSession;
        private LAPI_DOCUMENTS LivelinkDocuments;
        private LAPI_ATTRIBUTES LivelinkAttributes;
        private LivelinkInstance _currentInstanceSettings;
        private LLValue catVolCats;
        private CategoriesCollection instaceCategories;

        public int EnterpriseWorkspaceID;
        public int EnterpriseWorkspaceVolumeID;
        public int CategoriesWorkspaceID;
        public int CategoriesWorkspaceVolumeID;

        public string EnterpriseWorkspaceName;

        public Guid InstanceID
        {
            get 
            {
                if (_id == Guid.Empty)
                {
                    _id = Guid.NewGuid();
                }
                return _id;
            }
        }

        #region Constructors
        /// <summary>
        /// Initialise LivelinkManager with Livelink instance.
        /// </summary>
        /// <param name="livelinkInstance">LivelinkInstance object populated with Livelink connection information</param>
        public LivelinkManager(LivelinkInstance livelinkInstance)
        {
            //create livelink session with the configured settings
            CreateSession(livelinkInstance);
            if (!livelinkInstance.LivelinkSingleSignOnEnabled && livelinkInstance.ImpersonateLivelinkUser!=null && livelinkInstance.ImpersonateLivelinkUser.Trim() != string.Empty)
            {
                ImpersonateUser(livelinkInstance.ImpersonateLivelinkUser);
            }
        }
        public void ImpersonateUser(string userID)
        {
            //impersonate user
            LivelinkSession.ImpersonateUser(userID);

        }
        #endregion

        #region Private Methods
        private void CheckStatus(int status)
        {
            if (status != 0)
            {
                string messageString = string.Empty;
                int stat = 0;
                string message = string.Empty;
                string errMsg = string.Empty;
                string apiError = string.Empty;

                messageString = "Status=" + status + " (0x" + Convert.ToString(status, 16) + ")" + Environment.NewLine;
                if (LivelinkSession != null)
                {
                    stat = LivelinkSession.getStatus();
                    message = LivelinkSession.getStatusMessage();
                    errMsg = LivelinkSession.getErrMsg();
                    apiError = LivelinkSession.getApiError();

                    messageString = messageString +
                        "Session failure status is " + stat + " (0x" + Convert.ToString(stat, 16) + ")" + Environment.NewLine +
                        "Message: " + message + Environment.NewLine +
                        "errMsg: " + errMsg + Environment.NewLine +
                        "apiError: " + apiError + Environment.NewLine;
                }
                switch (stat)
                {
                    case 99:
                        throw new LivelinkSessionCreationException(messageString);
                    //case 107105:
                    //    throw new LivelinkBlankCategoryException(messageString);
                    case 107205:
                        throw new LivelinkNodeCategoryMissingException(messageString);
                    case 8000704:
                        throw new LivelinkAttributeDoesNotExistException(messageString);
                    case 103101:
                        throw new LivelinkNotEnoughPermissionsException(messageString);
                    case 101104:
                        if (errMsg.StartsWith("You do not have permission"))
                        {
                            throw new LivelinkNotEnoughPermissionsException(messageString);
                        }
                        else if (errMsg.StartsWith("Name cannot contain the character"))
                        {
                            throw new UnhandledLivelinkException(messageString);
                        }
                        else
                        {
                            throw new LivelinkNodeExistsException(messageString);
                        }
                    case 101103:
                        if (apiError == "Could not create path.")
                        {
                            throw new LivelinkNodeExistsException(messageString);
                        }
                        else
                        {
                            throw new LivelinkMandatoryAttributeMissingException(messageString);
                        }
                    default:
                        throw new UnhandledLivelinkException(messageString);
                }
            }
        }
        private void CreateSession(LivelinkInstance settings)
        {
            _currentInstanceSettings = settings;
            try
            {
                if (_currentInstanceSettings.LivelinkSingleSignOnEnabled)
                {
                    LLValue NTLMConfig = new LLValue(LLValue.LL_ASSOC);

                    NTLMConfig = new LLValue().setAssocNotSet();
                    NTLMConfig.add("HTTPS", LLValue.LL_FALSE);
                    NTLMConfig.add("LivelinkCGI", new LLValue(_currentInstanceSettings.LivelinkCGI));
                    if (_currentInstanceSettings.HTTPUserName != null && _currentInstanceSettings.HTTPUserName != string.Empty) NTLMConfig.add("HTTPUserName", new LLValue(_currentInstanceSettings.HTTPUserName));
                    if (_currentInstanceSettings.HTTPPassword != null && _currentInstanceSettings.HTTPPassword != string.Empty) NTLMConfig.add("HTTPPassword", new LLValue(_currentInstanceSettings.HTTPPassword));
                    NTLMConfig.add("VerifyServer", LLValue.LL_FALSE);
                    //NTLMConfig.add("DomainName", new LLValue(string.Empty));
                    LivelinkSession = new LLSession(_currentInstanceSettings.LivelinkServer, Convert.ToInt32(_currentInstanceSettings.LivelinkPortNo), string.Empty, string.Empty, string.Empty, NTLMConfig);
                }
                else
                {
                    string lluser = _currentInstanceSettings.LivelinkUserName;
                    string llpassword = _currentInstanceSettings.LivelinkUserPassword;

                    try
                    {
                        lluser = StringEncryptor.DecryptData(lluser);
                        llpassword = StringEncryptor.DecryptData(llpassword);
                    }
                    catch
                    { }

                    LivelinkSession = new LLSession(_currentInstanceSettings.LivelinkServer, Convert.ToInt32(_currentInstanceSettings.LivelinkPortNo), string.Empty, lluser, llpassword);
                }
                LivelinkDocuments = new LAPI_DOCUMENTS(LivelinkSession);
                LivelinkAttributes = new LAPI_ATTRIBUTES(LivelinkSession);


                LLValue EnterpriseWorkspace = new LLValue().setAssocNotSet();
                LLValue CategoriesWorkspace = new LLValue().setAssocNotSet();

                CheckStatus(LivelinkDocuments.AccessEnterpriseWS(EnterpriseWorkspace));
                CheckStatus(LivelinkDocuments.AccessCategoryWS(CategoriesWorkspace));
                EnterpriseWorkspaceID = EnterpriseWorkspace.toInteger("ID");
                EnterpriseWorkspaceVolumeID = EnterpriseWorkspace.toInteger("VolumeID");
                EnterpriseWorkspaceName = EnterpriseWorkspace.toString("Name");
                CategoriesWorkspaceID = CategoriesWorkspace.toInteger("ID");
                CategoriesWorkspaceVolumeID = CategoriesWorkspace.toInteger("VolumeID");
                System.Diagnostics.Debug.WriteLine(string.Format("GetCategoryVolumeCategories()-IN... {0}", DateTime.Now));
                catVolCats = GetCategoryVolumeCategories();
                instaceCategories = GetCategoryDefsOnly(catVolCats);
                System.Diagnostics.Debug.WriteLine(string.Format("GetCategoryVolumeCategories()-OUT... {0}", DateTime.Now));
            }
            catch (LLCouldNotConnectHTTPException conex)
            {
                throw new LivelinkConnectionException(conex.Message);
            }
            catch (LLIOException ex)
            {
                throw new LivelinkConnectionException(ex.Message);
            }
            catch (UnhandledLivelinkException)
            {
                throw;
            }
        }
        private LLValue GetCategoryVolumeCategories()
        {
            LLValue categories = new LLValue();

            CheckStatus(LivelinkDocuments.ListObjects(CategoriesWorkspaceVolumeID,
                CategoriesWorkspaceID,
                string.Empty,
                "(SubType=131)",
                LAPI_DOCUMENTS.PERM_SEE,
                categories));
            return categories;
        }
        private LLValue GetCategoryVersion(int categoryID)
        {
            LLValue categoryVersion = new LLValue();
            LLValue catID = new LLValue().setAssocNotSet();
            catID.add("ID", new LLValue(categoryID));
            catID.add("Version", new LLValue(0)); // 0 assumes the latest version 
            catID.add("Type", new LLValue(LAPI_ATTRIBUTES.CATEGORY_TYPE_LIBRARY));

            CheckStatus(LivelinkDocuments.FetchCategoryVersion(catID, categoryVersion));

            return categoryVersion;
        }
        private LLValue GetCategoryVersion(int objectID, int categoryID)
        {
            LLValue categoryVersion = new LLValue();
            LLValue catID = new LLValue().setAssocNotSet();
            catID.add("ID", new LLValue(categoryID));
            catID.add("Type", new LLValue(LAPI_ATTRIBUTES.CATEGORY_TYPE_LIBRARY));
            LLValue objID = new LLValue().setAssocNotSet();
            objID.add("ID", new LLValue(objectID));
            objID.add("Type", new LLValue(LAPI_DOCUMENTS.OBJECTTYPE));

            CheckStatus(LivelinkDocuments.GetObjectAttributesEx(objID, catID, categoryVersion));

            return categoryVersion;
        }
        private LLValue GetNodeCategories(int volumeID, int nodeID)
        {
            //Create the objID assoc necessary for some category functions
            LLValue objID = (new LLValue()).setAssocNotSet();
            objID.add("ID", nodeID);

            //First, obtain a list of the folder's categories
            LLValue catIDList = (new LLValue()).setAssocNotSet();
            CheckStatus(LivelinkDocuments.ListObjectCategoryIDs(objID, catIDList));

            return catIDList;
        }
        private LLValue GetAttributeNames(LLValue categoryVersion)
        {
            LLValue attributes = new LLValue();
            CheckStatus(LivelinkAttributes.AttrListNames(categoryVersion, null, attributes));

            return attributes;
        }
        private void SetAttributeValue(LLValue categoryVersion, Attribute attribute)
        {
            LLValue aValues = new LLValue().setList();
            aValues.setSize(1);
            bool doUpdate = false;
            object v = attribute.Value;
            if (v != null)
            {
                if (v is System.DateTime)
                {
                    DateTime d = (DateTime)v;
                    aValues.setDate(0, new java.util.Date(d.Year - 1900, d.Month - 1, d.Day));
                    doUpdate = true;
                }
                else if (v is System.String)
                {
                    string d = (string)v;
                    aValues.setString(0, d);
                    doUpdate = true;
                }
                else if (v is System.Int32)
                {
                    int d = (int)v;
                    aValues.setInteger(0, d);
                    doUpdate = true;
                }
                else if (v is System.Boolean)
                {
                    bool d = (bool)v;
                    aValues.setBoolean(0, d);
                    doUpdate = true;
                }
                else
                {
                    aValues.setString(0, v.ToString());
                    doUpdate = true;
                }
            }
            else
            {
                aValues.setUndefined(0);
                doUpdate = true;
            }
            try
            {
                if (doUpdate) CheckStatus(LivelinkAttributes.AttrSetValues(categoryVersion, attribute.Name, LAPI_ATTRIBUTES.ATTR_DATAVALUES, null, aValues));
            }
            catch (LivelinkAttributeDoesNotExistException ex)
            {
                throw new LivelinkAttributeDoesNotExistException(string.Format("{0} Attribute(Name:'{1}' Value:'{2}')", ex.Message, attribute.Name, attribute.Value));
            }

        }
        private LLValue GetAttribute(LLValue categoryVersion, string attributeName)
        {
            LLValue attribute = new LLValue();
            LLValue attrValues = new LLValue();
            LLValue catID = new LLValue().setAssocNotSet();
            CheckStatus(LivelinkAttributes.AttrGetInfo(categoryVersion, attributeName, null, attribute));
            CheckStatus(LivelinkAttributes.AttrGetValues(categoryVersion, attributeName, LAPI_ATTRIBUTES.ATTR_DEFAULTVALUES, null, attrValues));
            attribute.add("DefaultValues", attrValues);
            CheckStatus(LivelinkAttributes.AttrGetValues(categoryVersion, attributeName, LAPI_ATTRIBUTES.ATTR_DATAVALUES, null, attrValues));
            attribute.add("DataValues", attrValues);

            return attribute;

        }
        private LLValue GetNodes(int volumeID, int containerID, int subType)
        {
            LLValue nodes = new LLValue();

            CheckStatus(LivelinkDocuments.ListObjects(volumeID, containerID, string.Empty, string.Format("(SubType={0})", subType), LAPI_DOCUMENTS.PERM_SEE, nodes));

            return nodes;
        }
        private Node GetNode(Node node)
        {
            LLValue nodeInfo = new LLValue();
            CheckStatus(LivelinkDocuments.GetObjectInfo(node.VolumeID, node.ID, nodeInfo));

            node.ID = nodeInfo.toInteger("ID");
            node.Name = nodeInfo.toString("Name");
            node.VolumeID = nodeInfo.toInteger("VolumeID");
            node.ParentID = nodeInfo.toInteger("ParentID");
            java.util.Date cd = nodeInfo.toDate("CreateDate");
            node.CreatedDate = new DateTime(cd.getYear() + 1900, cd.getMonth() + 1, cd.getDate(), cd.getHours(), cd.getMinutes(), cd.getSeconds());
            java.util.Date md = nodeInfo.toDate("ModifyDate");
            node.ModifiedDate = new DateTime(md.getYear() + 1900, md.getMonth() + 1, md.getDate(), md.getHours(), md.getMinutes(), md.getSeconds());
            return GetNodeCategories(node);

        }
        private Node GetNodeCategories(Node node)
        {
            //get categories
            LLValue categories = GetNodeCategories(node.VolumeID, node.ID);
            node.Categories = GetCategoriesForNode(categories, node.ID);
            return node;
        }
        private FoldersCollection CreateTheFolderStructure(Folder parentFolder, FoldersCollection foldersInfo, NodeExistsCreateMode uploadMode)
        {
            FoldersCollection newFolders = new FoldersCollection();
            foreach (Folder folder in foldersInfo)
            {
                Folder newFolder = CreateTheFolder(parentFolder, folder, uploadMode);
                if (folder.Folders.Count > 0)
                {
                    newFolder.Folders = CreateTheFolderStructure(newFolder, folder.Folders, uploadMode);
                }
                newFolders.Add(newFolder);
            }
            return newFolders;
        }
        private FoldersCollection GetChildrenFolders(Folder folder)
        {
            FoldersCollection result = null;
            //get children
            LLValue children = GetNodes(folder.VolumeID, folder.ID, 0);
            FoldersCollection foders = new FoldersCollection();
            if (children != null)
            {
                result = new FoldersCollection();
                LLValueEnumeration folders = children.enumerateValues();
                while (folders.hasMoreElements())
                {
                     LLValue fl = folders.nextValue();
                     Folder fld = new Folder(fl.toInteger("VolumeID"), fl.toInteger("ID"));
                    result.Add((Folder)GetNode(fld));
                }
            }
            return result;

        }
        private CategoriesCollection GetCategoryDefsAndAttributes(LLValue categories)
        {
            CategoriesCollection result = new CategoriesCollection();
            //get categories
            if (categories != null)
            {
                LLValueEnumeration cats = categories.enumerateValues();
                while (cats.hasMoreElements())
                {
                    LLValue cat = cats.nextValue();
                    Category category = new Category();

                    category.ID = cat.toInteger("ID");
                    category.ParentID = cat.toInteger("ParentID");
                    category.VolumeID = cat.toInteger("VolumeID");
                    category.Name = cat.toString("Name");
                    category.Attributes = GetAttributes(category.ID, 0);
                    result.Add(category);
                }
            }
            return result;
        }
        private CategoriesCollection GetCategoriesForNode(LLValue categories, int nodeID)
        {
            CategoriesCollection result = new CategoriesCollection();
            //get categories
            if (categories != null && nodeID!=0)
            {
                LLValueEnumeration cats = categories.enumerateValues();
                while (cats.hasMoreElements())
                {
                    LLValue cat = cats.nextValue();
                    Category category = new Category();
                    category.ID = cat.toInteger("ID");
                    category.Name = cat.toString("DisplayName");
                    category.Inhertiance = cat.toBoolean("Inheritance");
                    category.Attributes = GetAttributes(category.ID, nodeID);
                    result.Add(category);
                }
            }
            return result;
        }
        private CategoriesCollection GetCategoryDefsOnly(LLValue categories)
        {
            CategoriesCollection result = new CategoriesCollection();
            //get categories
            if (categories != null)
            {
                LLValueEnumeration cats = categories.enumerateValues();
                while (cats.hasMoreElements())
                {
                    LLValue cat = cats.nextValue();
                    Category category = new Category();
                    category.ID = cat.toInteger("ID");
                    category.ParentID = cat.toInteger("ParentID");
                    category.VolumeID = cat.toInteger("VolumeID");
                    category.Name = cat.toString("Name");
                    result.Add(category);
                }
            }
            return result;
        }
        private AttributesCollection GetAttributes(int categoryID, int nodeID)
        {
            AttributesCollection result = new AttributesCollection();
            //get attributes
            LLValue categoryVersion;
            if (nodeID != 0)
            {
                categoryVersion = GetCategoryVersion(nodeID, categoryID);
            }
            else
            {
                categoryVersion = GetCategoryVersion(categoryID);
            }
            LLValue attributeNames = GetAttributeNames(categoryVersion);
            LLValueEnumeration attrNames = attributeNames.enumerateValues();
            while (attrNames.hasMoreElements())
            {
                LLValue attrName = attrNames.nextValue();
                LLValue attr = GetAttribute(categoryVersion, attrName.ToString());
                Attribute attribute = new Attribute(attr);

                result.Add(attribute);
            }
            return result;
        }
        private void ApplyCategory(int nodeID, LLValue categoryVersion)
        {
            LLValue objID = (new LLValue()).setAssocNotSet();
            objID.add("ID", nodeID);

            CheckStatus(LivelinkDocuments.SetObjectAttributesEx(objID, categoryVersion));
        }
        private Category FindCategory(Category cat)
        {
            return FindCategory(cat.Name);
        }
        private Category FindCategory(string catName)
        {
            Category c = instaceCategories[catName];
            return c;
        }
        private Category FindCategory(int catID)
        {
            foreach (Category cat in instaceCategories)
            {
                if (cat.ID == catID) return cat;
            }
            return null;
        }
        private Folder CreateTheFolder(Folder parentFolder, Folder folderInfo, NodeExistsCreateMode uploadMode)
        {
            if (folderInfo == null || folderInfo.Name == null || folderInfo.Name.Trim() == string.Empty) throw new LivelinkInvalidNodeException(string.Format("Supplied folder object is not valid. Folder name is blank."));

            LLValue createInfo = (new LLValue()).setAssocNotSet();
            LLValue objectInfo = new LLValue();
            LLValue folCategories = null;
            LLValue parentCategories = null;
            Folder folder = null;
            try
            {
                folCategories = (new LLValue()).setList();
                parentCategories = (new LLValue()).setList();
                Category catDef = null;
                //get parent categories
                foreach (Category cat in parentFolder.Categories)
                {
                    if (cat.ID == 0 && cat.Name.Trim() != string.Empty)
                    {
                        catDef = FindCategory(cat);
                        if (catDef == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                    }
                    else
                    {
                        if (cat.Inhertiance)
                        {
                            catDef = cat;
                        }
                        else
                        {
                            catDef = FindCategory(cat);
                            if (catDef == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                        }
                    }
                    LLValue categoryVersion = null;
                    if (cat.Inhertiance)
                    {
                        categoryVersion = GetCategoryVersion(parentFolder.ID, catDef.ID);
                        if (categoryVersion == null) throw new UnhandledLivelinkException(string.Format("Cannot fetch category version - Parent Folder(ID:'{0}' Name:'{1}'), Category (ID:'{2}' Name:'{3}')", parentFolder.ID, parentFolder.Name, catDef.ID, catDef.Name));
                    }
                    else
                    {
                        categoryVersion = GetCategoryVersion(catDef.ID);
                        if (categoryVersion == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                    }

                    parentCategories.add(categoryVersion);
                }

                //get folder categories
                foreach (Category cat in folderInfo.Categories)
                {
                    LLValue categoryVersion = null;
                    LLValueEnumeration cen = parentCategories.enumerateValues();
                    while (cen.hasMoreElements())
                    {
                        LLValue cv = cen.nextValue();
                        if (cv.toValue("catID").toInteger("ID") == cat.ID || cv.toValue("definition").toString("DisplayName") == cat.Name)
                        {
                            categoryVersion = cv;
                            break;
                        }
                    }
                    if (cat.ID == 0)
                    {
                        catDef = FindCategory(cat);
                        if (catDef == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                    }
                    else
                    {
                        catDef = cat;
                    }
                    if (categoryVersion == null) categoryVersion = GetCategoryVersion(catDef.ID);
                    if (categoryVersion == null) throw new UnhandledLivelinkException(string.Format("Cannot fetch category version - (Category (ID:'{0}' Name:'{1}')", catDef.ID, catDef.Name));
                    foreach (Attribute attr in cat.Attributes)
                    {
                        SetAttributeValue(categoryVersion, attr);
                    }
                    folCategories.add(categoryVersion);
                }

                createInfo.add("Categories", folCategories);

                CheckStatus(LivelinkDocuments.CreateObjectEx(parentFolder.VolumeID, parentFolder.ID, LAPI_DOCUMENTS.OBJECTTYPE, LAPI_DOCUMENTS.FOLDERSUBTYPE, folderInfo.Name, createInfo, objectInfo));
                folder = new Folder();
                folder.ID = objectInfo.toInteger("ID");
                folder.ParentID = objectInfo.toInteger("ParentID");
                folder.VolumeID = objectInfo.toInteger("VolumeID");
                folder.Name = objectInfo.toString("Name");
                folder.Categories = folderInfo.Categories;
            }
            catch (LivelinkNodeExistsException ex) //folder exists
            {
                switch (uploadMode)
                {
                    case NodeExistsCreateMode.Overwrite: //find the folder and update its categories
                        LLValue folders = new LLValue();

                        CheckStatus(LivelinkDocuments.ListObjects(parentFolder.VolumeID, parentFolder.ID, string.Empty, "(SubType=" + LAPI_DOCUMENTS.FOLDERSUBTYPE + ")", LAPI_DOCUMENTS.PERM_SEE, folders));
                        LLValueEnumeration en = folders.enumerateValues();
                        while (en.hasMoreElements())
                        {
                            LLValue fol = en.nextValue();
                            if (fol.toString("Name") == folderInfo.Name)
                            {
                                folder = new Folder();
                                folder.ID = fol.toInteger("ID");
                                folder.ParentID = fol.toInteger("ParentID");
                                folder.VolumeID = fol.toInteger("VolumeID");
                                folder.Name = fol.toString("Name");
                                folder.Categories = folderInfo.Categories;
                                LLValueEnumeration cats = folCategories.enumerateValues();
                                while (cats.hasMoreElements())
                                {
                                    ApplyCategory(folder.ID, cats.nextValue());
                                }
                                break;
                            }
                        }
                        break;
                    case NodeExistsCreateMode.ReturnError:
                        LLValue folders1 = new LLValue();
                        int nodeID = 0;
                        int volumeID = 0;
                        CheckStatus(LivelinkDocuments.ListObjects(parentFolder.VolumeID, parentFolder.ID, string.Empty, "(SubType=" + LAPI_DOCUMENTS.FOLDERSUBTYPE + ")", LAPI_DOCUMENTS.PERM_SEE, folders1));
                        LLValueEnumeration en1 = folders1.enumerateValues();
                        while (en1.hasMoreElements())
                        {
                            LLValue fol = en1.nextValue();
                            if (fol.toString("Name").TrimEnd() == folderInfo.Name.TrimEnd())
                            {
                                nodeID = fol.toInteger("ID");
                                volumeID = fol.toInteger("VolumeID");
                                break;
                            }
                        }
                        throw new LivelinkNodeExistsException(ex.Message, nodeID, volumeID);
                    case NodeExistsCreateMode.Read:
                        LLValue folders2 = new LLValue();
                        CheckStatus(LivelinkDocuments.ListObjects(parentFolder.VolumeID, parentFolder.ID, string.Empty, "(SubType=" + LAPI_DOCUMENTS.FOLDERSUBTYPE + ")", LAPI_DOCUMENTS.PERM_SEE, folders2));
                        LLValueEnumeration en2 = folders2.enumerateValues();
                        while (en2.hasMoreElements())
                        {
                            LLValue fol = en2.nextValue();
                            if (fol.toString("Name").TrimEnd() == folderInfo.Name.TrimEnd())
                            {
                                folderInfo.ID = fol.toInteger("ID");
                                folderInfo.VolumeID = fol.toInteger("VolumeID");
                                break;
                            }
                        }
                        folder = new Folder();

                        folder = (Folder)GetNode(folderInfo);
                        break;
                    default:
                        break;
                }

            }

            return folder;
        }
        private Node UpdateNode(Node node)
        {
            LLValue updateInfo = (new LLValue()).setAssocNotSet();
            LLValue categories = null;
            Category catDef = null;
            //update info
            categories = (new LLValue()).setList();
            foreach (Category cat in node.Categories)
            {
                if (cat.ID == 0)
                {
                    catDef = FindCategory(cat);
                    if (catDef == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                }
                else
                {
                    catDef = cat;
                }
                LLValue categoryVersion = null;
                try
                {
                    categoryVersion = GetCategoryVersion(node.ID, catDef.ID);
                }
                catch (LivelinkNodeCategoryMissingException) //it will throw exception if the category is not applied to the node
                {
                    //do nothing, to allow as to apply the category
                }
                if (categoryVersion == null) categoryVersion = GetCategoryVersion(catDef.ID);
                if (categoryVersion == null) throw new UnhandledLivelinkException(string.Format("Cannot fetch category version - (Category (ID:'{0}' Name:'{1}')", catDef.ID, catDef.Name));
                foreach (Attribute attr in cat.Attributes)
                {
                    SetAttributeValue(categoryVersion, attr);
                }
                categories.add(categoryVersion);
            }
            updateInfo.add("Categories", categories);
            //update object
            CheckStatus(LivelinkDocuments.UpdateObjectInfo(node.VolumeID, node.ID, updateInfo));
            //apply cats
            LLValueEnumeration cats = categories.enumerateValues();
            while (cats.hasMoreElements())
            {
                ApplyCategory(node.ID, cats.nextValue());
            }
            return GetNode(node);
        }
        private Node ValidateInputNode(Node node)
        {
            if (node.ID == 0) throw new LivelinkInvalidNodeException(string.Format("Supplied node object is not valid. node ID is missing."));

            if (node.VolumeID == 0) throw new LivelinkInvalidNodeException(string.Format("Invalid node ID:{0}. Node with that Object ID does not exist.", node.ID));

            return node;
        }
        private Document AddTheDocumentAndVersionWithFileContent(Folder parentFolder, Document documentInfo, string fileName, byte[] fileContent, NodeExistsCreateMode uploadMode)
        {
            Document document = AddTheDocument(parentFolder, documentInfo, uploadMode);
            try
            {
                LLValue versionInfo = AddTheVersion(document.VolumeID, document.ID, fileName, fileContent);
            }
            catch (UnhandledLivelinkException ex)
            {
                try
                {
                    CheckStatus(LivelinkDocuments.DeleteObject(document.VolumeID, document.ID));
                }
                catch (UnhandledLivelinkException)
                { }
                throw ex;
            }

            return document;
        }
        private Document AddTheDocument(Folder parentFolder, Document documentInfo, NodeExistsCreateMode uploadMode)
        {
            if (documentInfo == null || documentInfo.Name == null || documentInfo.Name.Trim() == string.Empty) throw new LivelinkInvalidNodeException(string.Format("Supplied document object is not valid. Document name is blank."));
            LLValue createInfo = (new LLValue()).setAssocNotSet();
            LLValue objectInfo = new LLValue();
            LLValue versionInfo = new LLValue();
            LLValue docCategories = null;
            LLValue parentCategories = null;

            Document document = null;

            try
            {
                docCategories = (new LLValue()).setList();
                parentCategories = (new LLValue()).setList();
                Category catDef = null;
                //get parent categories
                foreach (Category cat in parentFolder.Categories)
                {
                    if (cat.ID == 0 && cat.Name.Trim() != string.Empty)
                    {
                        catDef = FindCategory(cat);
                        if (catDef == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                    }
                    else
                    {
                        if (cat.Inhertiance)
                        {
                            catDef = cat;
                        }
                        else
                        {
                            catDef = FindCategory(cat);
                            if (catDef == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                        }
                    }
                    LLValue categoryVersion = null;
                    if (cat.Inhertiance)
                    {
                        categoryVersion = GetCategoryVersion(parentFolder.ID, catDef.ID);
                        if (categoryVersion == null) throw new UnhandledLivelinkException(string.Format("Cannot fetch category version - Parent Folder(ID:'{0}' Name:'{1}'), Category (ID:'{2}' Name:'{3}')", parentFolder.ID, parentFolder.Name, catDef.ID, catDef.Name));
                    }
                    else
                    {
                        categoryVersion = GetCategoryVersion(catDef.ID);
                        if (categoryVersion == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                    }

                    parentCategories.add(categoryVersion);
                }
                //get doc categories
                foreach (Category cat in documentInfo.Categories)
                {
                    LLValue categoryVersion = null;
                    LLValueEnumeration cen = parentCategories.enumerateValues();
                    while (cen.hasMoreElements())
                    {
                        LLValue cv = cen.nextValue();
                        if (cv.toValue("catID").toInteger("ID") == cat.ID || cv.toValue("definition").toString("DisplayName") == cat.Name)
                        {
                            categoryVersion = cv;
                            break;
                        }
                    }
                    if (cat.ID == 0)
                    {
                        catDef = FindCategory(cat);
                        if (catDef == null) throw new LivelinkCategoryDoesNotExistException(string.Format("Category(ID:'{0}' Name:'{1}') does not exist in Categories Volume (ID:'{2}' VolumeID:'{3}')", cat.ID, cat.Name, CategoriesWorkspaceID, CategoriesWorkspaceVolumeID));
                    }
                    else
                    {
                        catDef = cat;
                    }
                    if (categoryVersion == null) categoryVersion = GetCategoryVersion(catDef.ID);
                    if (categoryVersion == null) throw new UnhandledLivelinkException(string.Format("Cannot fetch category version - (Category (ID:'{0}' Name:'{1}')", catDef.ID, catDef.Name));
                    foreach (Attribute attr in cat.Attributes)
                    {
                        SetAttributeValue(categoryVersion, attr);
                    }
                    docCategories.add(categoryVersion);
                }

                //create info attach categories
                createInfo.add("Categories", docCategories);

                //create the document
                CheckStatus(LivelinkDocuments.CreateObjectEx(parentFolder.VolumeID, parentFolder.ID, LAPI_DOCUMENTS.OBJECTTYPE, LAPI_DOCUMENTS.DOCUMENTSUBTYPE, documentInfo.Name, createInfo, objectInfo));

                document = new Document();
                document.ID = objectInfo.toInteger("ID");
                document.ParentID = objectInfo.toInteger("ParentID");
                document.VolumeID = objectInfo.toInteger("VolumeID");
                document.Name = objectInfo.toString("Name");
                document.Categories = documentInfo.Categories;

            }
            catch (LivelinkNodeExistsException ex) //document exists
            {
                switch (uploadMode)
                {
                    case NodeExistsCreateMode.Overwrite: //find the document and update its categories
                        LLValue documents = new LLValue();

                        CheckStatus(LivelinkDocuments.ListObjects(parentFolder.VolumeID, parentFolder.ID, string.Empty, "(SubType=" + LAPI_DOCUMENTS.DOCUMENTSUBTYPE + ")", LAPI_DOCUMENTS.PERM_SEE, documents));
                        LLValueEnumeration en = documents.enumerateValues();
                        while (en.hasMoreElements())
                        {
                            LLValue doc = en.nextValue();
                            if (doc.toInteger("ID") == documentInfo.ID || doc.toString("Name") == documentInfo.Name)
                            {
                                document = new Document();
                                document.ID = doc.toInteger("ID");
                                document.ParentID = doc.toInteger("ParentID");
                                document.VolumeID = doc.toInteger("VolumeID");
                                document.Name = doc.toString("Name");
                                document.Categories = documentInfo.Categories;
                                LLValueEnumeration cats = docCategories.enumerateValues();
                                while (cats.hasMoreElements())
                                {
                                    ApplyCategory(document.ID, cats.nextValue());
                                }
                                break;
                            }
                        }
                        if (document == null) throw new LivelinkNodeDoesNotExistException(string.Format("Document(ID:'{0}' Name:'{1}') does not exist in folder (ID:'{2}' Name:'{3}')", documentInfo.ID, documentInfo.Name, parentFolder.ID, parentFolder.Name));
                        break;
                    case NodeExistsCreateMode.Read:
                        //read the document
                        break;
                    case NodeExistsCreateMode.ReturnError:
                        throw new LivelinkNodeExistsException(ex.Message);
                    default:
                        break;
                }

            }

            return document;
        }
        private LLValue AddTheVersion(int volumeID, int documentID, string fileName, byte[] fileContent)
        {
            //ValidateFileName(fileName);
            if (fileContent == null) throw new UnhandledLivelinkException("Null file content.");

            LLValue versionInfo = new LLValue();
            LLValue fileInfo = new LLValue().setAssocNotSet();
            //there is a bug in LAPI when using NTLM authentication
            //the adding of version by passing BinaryReader fails with System.ObjectDisposedException
            //https://knowledge.opentext.com/knowledge/llisapi.dll?func=ll&objId=11261803&objAction=view&show=2
            if (!_currentInstanceSettings.LivelinkSingleSignOnEnabled)
            {
                try
                {
                    Stream stream = new MemoryStream(fileContent);
                    BinaryReader reader = new BinaryReader(stream);
                    //add version
                    CheckStatus(LivelinkDocuments.CreateVersion(volumeID, documentID, fileName, reader, fileContent.Length, fileInfo, versionInfo));

                    return versionInfo;
                }
                catch (System.ObjectDisposedException)
                {
                    throw new UnhandledLivelinkException("Failed to add version. There is LAPI bug with NTLM!");
                }
            }
            else
            {
                //LLValue versionInfo = new LLValue();

                string outputFileDirectory = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
                Directory.CreateDirectory(outputFileDirectory);
                string outputFileName = Path.Combine(outputFileDirectory, fileName);

                System.IO.FileStream outFile = null;
                //Get content string to a temp file for upload
                try
                {

                    //writes the byte array into the file
                    outFile = new System.IO.FileStream(outputFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    outFile.Write(fileContent, 0, fileContent.Length);
                }
                catch (System.Exception exp)
                {
                    // Error creating stream or writing to it.
                    throw exp;
                }
                finally
                {
                    if (outFile != null) outFile.Close();
                }

                //add version
                CheckStatus(LivelinkDocuments.CreateVersion(volumeID, documentID, outputFileName, versionInfo));

                //delete temp file
                Directory.Delete(outputFileDirectory, true);

                return versionInfo;
            }
        }
        //private void ValidateFileName(string fileName)
        //{
        //    bool invalid = false;
        //    if (fileName == null) invalid = true;
        //    if (!invalid && fileName.Trim() == string.Empty) invalid = true;
        //    if (!invalid)
        //    {
        //        char[] invalidFileNameChars = @"<>:""/\|?*".ToCharArray();
        //        for (int i = 0; i < invalidFileNameChars.Length; i++)
        //        {
        //            if (fileName.IndexOf(invalidFileNameChars[i]) >= 0)
        //            {
        //                invalid = true;
        //                break;
        //            }
        //        }
        //    }
        //    if (!invalid)
        //    {
        //        FileInfo fi = new FileInfo(fileName);
        //        invalid = ValidateFileExtension(fi.Extension);
        //    }

        //    if (invalid) throw new UnhandledLivelinkException(string.Format(@"File name:'{0}' is invalid. File name cannot contain '< > : "" / \ | ? *' and must have a valid file extension. Example '.pdf''", fileName));
        //}
        //private bool ValidateFileExtension(string extension)
        //{
        //    if (extension.Trim() == string.Empty) return true;
        //    System.Security.Permissions.RegistryPermission regPerm = new System.Security.Permissions.RegistryPermission(System.Security.Permissions.RegistryPermissionAccess.Read, "\\HKEY_CLASSES_ROOT");
        //    Microsoft.Win32.RegistryKey classesRoot = Microsoft.Win32.Registry.ClassesRoot;
        //    Microsoft.Win32.RegistryKey typeKey = classesRoot.OpenSubKey(@"MIME\Database\Content Type");
        //    foreach (string keyname in typeKey.GetSubKeyNames())
        //    {
        //        Microsoft.Win32.RegistryKey curKey = classesRoot.OpenSubKey(@"MIME\Database\Content Type\" + keyname);
        //        if (curKey != null && curKey.GetValue("Extension") != null && curKey.GetValue("Extension").ToString().ToLower() == extension.ToLower())
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns all categories defined in the Livelink instance Category Volume
        /// </summary>
        /// <returns></returns>
        public CategoriesCollection GetInstanceCategories()
        {
            return instaceCategories;
        }
        /// <summary>
        /// Get a folder by its object id
        /// </summary>
        /// <param name="folderID">folder ID</param>
        /// <returns>Folder object</returns>
        public Folder GetFolder(int folderID)
        {
            bool invalid = false;
            if (folderID == 0) invalid = true;
            //if (!invalid)
            //{
            //    int subType = dbManager.GetNodeSubType(folderID, folderVolumeID);
            //    if (subType != LAPI_DOCUMENTS.FOLDERSUBTYPE) invalid = true;
            //}
            if (invalid) throw new LivelinkNodeDoesNotExistException(string.Format("Folder(ID:'{0}' VolumeID:'{1}')", folderID, EnterpriseWorkspaceVolumeID));

            Folder folder = new Folder(EnterpriseWorkspaceVolumeID, folderID);
            return (Folder)GetNode(folder);
        }
        /// <summary>
        /// Create folder object
        /// </summary>
        /// <param name="parentID">parent ID</param>
        /// <param name="folderInfo">folder object containg all update information and categories to apply to the folder</param>
        /// <param name="uploadMode">overwrite if exists or throw error</param>
        /// <returns></returns>
        public Folder CreateFolder(int parentID, Folder folderInfo, NodeExistsCreateMode uploadMode)
        {
            Folder parentFolder = GetFolder(parentID);
            Folder folder = CreateTheFolder(parentFolder, folderInfo, uploadMode);
            return folder;
        }
        /// <summary>
        /// Create folder objects
        /// </summary>
        /// <param name="parentID">parent ID</param>
        /// <param name="foldersInfo">folders collection object containg all folders update information and categories to apply to the folders</param>
        /// <param name="uploadMode">overwrite if exists or throw error</param>
        /// <returns></returns>
        public FoldersCollection CreateFolderStructure(int parentID, FoldersCollection foldersInfo, NodeExistsCreateMode uploadMode)
        {
            Folder parentFolder = GetFolder(parentID);
            FoldersCollection folders = CreateTheFolderStructure(parentFolder, foldersInfo, uploadMode);
            return folders;
        }
        /// <summary>
        /// Add document object into folder container in Livelink.
        /// </summary>
        /// <param name="parentID">parent folder ID</param>
        /// <param name="documentInfo">Document object containg all update information and categories to apply to the document</param>
        /// <param name="fileName">file name to be used as the version file added to the document object</param>
        /// <param name="fileContent">version file content</param>
        /// <param name="uploadMode">overwrite if exists or throw error</param>
        /// <returns>Updated Document object</returns>
        public Document AddDocument(int parentID, Document documentInfo, string fileName, byte[] fileContent, NodeExistsCreateMode uploadMode)
        {
            Folder folder = GetFolder(parentID);
            Document document = AddTheDocumentAndVersionWithFileContent(folder, documentInfo, fileName, fileContent, uploadMode);
            return document;
        }
        public void UpdateDocumentCreatedByDate(int documentID, DateTime createdDate, string createdByUser)
        {
            LLValue objectInfo = (new LLValue()).setAssocNotSet();

            //created date
            objectInfo.add("CreateDate", new java.util.Date(createdDate.Year - 1900, createdDate.Month - 1, createdDate.Day));

            ImpersonateUser(createdByUser);

            CheckStatus(LivelinkDocuments.UpdateObjectInfo(EnterpriseWorkspaceVolumeID, documentID, objectInfo));
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            LivelinkDocuments = null;
            LivelinkAttributes = null;
            LivelinkSession = null;
        }

        #endregion

    }
}
